// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import PfSpriteToast from "./PfSpriteToast";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    pfSpriteToast: cc.Prefab = null;

    showToast() {
        let ndToast = cc.instantiate(this.pfSpriteToast);
        ndToast.getComponent(PfSpriteToast).setData({ count: 1000 });
        ndToast.parent = this.node;
    }

    // update (dt) {}
}
