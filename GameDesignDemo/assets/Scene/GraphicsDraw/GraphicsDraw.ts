// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class GraphicsDraw extends cc.Component {

    @property(cc.Graphics)
    graphics: cc.Graphics = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.graphics.lineWidth = 5;

        this.graphics.fillColor.fromHEX("#ffff00");

        this.graphics.moveTo(0, 0);

        this.graphics.lineTo(-100, 20);

        this.graphics.lineTo(100, 20);

        this.graphics.close();
        this.graphics.stroke();
        this.graphics.fill();
    }

    // update (dt) {}
}
