

import AdaptUtils from "./adaptUtils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopView extends cc.Component {

    @property(cc.Node)
    ndUiRoot: cc.Node = null;

    onLoad() {
        if (AdaptUtils.isLongScreen()) {
            cc.log("需要适配！！");
            let widget: cc.Widget = this.ndUiRoot.getComponent(cc.Widget);
            if (!widget) {
                widget = this.ndUiRoot.addComponent(cc.Widget);
            }
            //将会用到left right top bottom，所以这样设置
            widget.isAlignBottom = true;
            widget.isAlignLeft = true;
            widget.isAlignRight = true;
            widget.isAlignTop = true;

            widget.left = AdaptUtils.getLiuhaiWidthPx();
        }
    }

    onClickBtnClose() {
        this.node.destroy();
    }


    // update (dt) {}
}
