

import FrameAnimation from "./FrameAnimation";

const { ccclass, property } = cc._decorator;

@ccclass
export default class FrameAnimationScene extends cc.Component {

    @property(cc.Node)
    ndSprite: cc.Node = null;

    @property(cc.SpriteFrame)
    spriteFrames: cc.SpriteFrame[] = [];

    start() {
        this.ndSprite.getComponent(FrameAnimation).setData(this.spriteFrames, 0.5);
        this.ndSprite.getComponent(FrameAnimation).play();
    }
}
